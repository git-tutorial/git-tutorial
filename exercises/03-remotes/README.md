# Working with remote repositories

1. Some preliminary checks:

    a. In your local working copy, check that no remote repository is already configured:
      ```
      git remote
      ```
    b. Move to another directory, out of the `dummymaths` one, and initialize there a bare repository. We will use it as a remote repository for `dummymaths`
      ```
      cd ..
      mkdir dummymaths_remote
      cd dummymaths_remote
      git init --bare
      ```
2. Move back to the `dummymaths` directory, that contains your initial git working copy and from there add the newly created remote repository. The url of this repository is just a path in your filesystem:
  ```
  git remote add origin <path to dummymaths_remote>
  ```
3. Push your `main` branch and enable upstream tracking in the meantime:
  ```
  git push origin main -u
  ```
4. Check that the `main` branch is now referenced on the remote repository:
  ```
  git remote show origin
  ```
5. In another directory, clone the repository of the source code of the tutorial that is hosted on gitlab:
  ```
  cd ..
  git clone https://gitlab.inria.fr/git-tutorial/git-tutorial.git
  ```
  You can check the status of your local copy and the information about the remote repository:
  ```
  cd git-tutorial
  git status
  git remote -v
  ```

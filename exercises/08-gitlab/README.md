# Working with GitLab

In this exercise, you'll fork the existing `dummymaths` repository on
[gitlab.inria.fr](https://gitlab.inria.fr), extend it and propose your changes
upstream using a merge request.

The changes that can be applied are:
- adding a `modulo` function and its associated test
- updating the README.md function

## Setup your Gitlab account

1. Check that you can login to https://gitlab.inria.fr
2. If not done already, add a public SSH key to your [Gitlab account](https://gitlab.inria.fr/-/profile/keys)
3. Check that you can access the [dummymaths project](https://gitlab.inria.fr/git-tutorial/dummymaths) on Gitlab

## Setup your local repository

1. Clone the upstream project on your machine:
  ```
  git clone git@gitlab.inria.fr:git-tutorial/dummymaths.git dummymaths-remote
  ```
2. On the Gitlab web interface, [fork the project](https://gitlab.inria.fr/git-tutorial/dummymaths/-/forks/new)
3. Add your fork as a new remote in your local copy:
  ```
  cd dummymaths-remote
  git remote add <fork name> git@gitlab.inria.fr:<account name>/dummymaths.git
  ```
If you are an external user, you won't be able to fork the project in your personal space.
But you can fork in a dedicated subgroup created for the session training.
For example, it may be `git-tutorial/session-2023-05` if the session occurs in May 2023.
In this case, on the Gitlab web interface, select namespace `git-tutorial/session-2023-05`
and project name with a prefix to avoid collisions with other users' forks.
  ```
  cd dummymaths-remote
  git remote add <fork name> git@gitlab.inria.fr:git-tutorial/session-2023-05/<prefix>-dummymaths.git
  ```

4. Check the configuration of your remotes:
  ```
  git remote -v
  ```

## Implement the new `modulo` function

1. First create a dedicated branch where you'll implement the new function:
  ```
  git checkout -b modulo
  ```
2. Add the following content to the `myfuncs.py` module:
  ```python
  def modulo(a, b):
      """Return a modulo b."""
      try:
          return a % b
      except ZeroDivisionError:
          return None
  ```
  Commit the changes:
  ```
  git commit -am "myfuncs: add modulo function"
  ```
3. Add the matching test function in the `test_myfuncs.py`:
  ```python
  # Add this import line
  from myfuncs import modulo

  [...]

  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, None),
          (0, 42, 0),
          (42, 0, None),
          (42, 1, 0),
          (1, 42, 1),
          (10, 3, 1),
          (10, 5, 0),
      ]
  )
  def test_modulo(a, b, res):
      assert modulo(a, b) == res
  ```
  Commit the changes:
  ```
  git commit -am "tests: add test for modulo function"
  ```
4. Check that the tests are passing:
  ```
  pytest
  ```

## Open the merge-request

1. First you have to push the `modulo` branch to **your fork**:
  ```
  git push <fork name> modulo
  ```
2. The command above should propose you if you want to open a merge request. The
  answer is obviously yes. You can either:
  - click on the proposed link
  - click on the web interface
3. In the web interface, do a last minute check of your changes (everything is
  there ?, no typo ?).
4. If everything is fine, click the "Open merge-request" button, and wait for
  the reviews
5. Update the README.md file in a separate branch (created from `main`), commit
  your changes and open another merge request.

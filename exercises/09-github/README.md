# Working with GitHub

In this exercise, you'll fork the existing `dummymaths` repository on
[GitHub](https://github.com), extend it like in exercise `08-gitlab` and
propose your changes upstream using a pull request.

## Setup your GitHub account

1. Check that you can login to https://github.com
2. If not done already, add a public SSH key to your [GitHub account](https://github.com/settings/keys)
3. Check that you can access the [dummymaths project](https://github.com/aabadie/dummymaths) on GitHub

## Setup your local repository

Here you'll reuse the `dummymaths` cloned in the `dummymaths-remote` directory
from the previous exercise on GitLab.

1. On the [GitHub web interface](https://github.com/aabadie/dummymaths), fork
  the project by clicking the fork button on the top right corner
2. Add your fork as a new remote in your local copy (use your login on github, or just `github` as fork name):
  ```
  git remote add <fork name> git@github.com:<github login>/dummymaths.git
  ```
4. Check the configuration of your remotes:
  ```
  git remote -v
  ```
  You should have:
  - origin: the upstream repository on gitlab
  - `<fork name>`: your fork on Gitlab
  - `<github fork name>`: your fork on GitHub

## Open the pull-request

1. Push the existing `modulo` branch to your github fork:
  ```
  git push <github fork name> modulo
  ```
2. The command above should propose you if you want to open a pull request. The
  answer is obviously yes. You can either:
  - click on the proposed link
  - click on the web interface. There's a link in a header of the
    [dummymaths project](https://github.com/aabadie/dummymaths)
3. In the web interface, do a last minute check of your changes (everything is
  there ?, no typo ?).
4. If everything is fine, click the "Open pull-request" green button, and wait
  for the reviews

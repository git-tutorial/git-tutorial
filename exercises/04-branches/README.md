# Manipulating branches

1. Move back to the `dummymaths` directory and list your local and remote
  branches:
  ```
  git branch
  git branch -a
  ```
2. Create a branch called `multiply` and list the branches again:
  ```
  git branch multiply
  git branch
  ```
  Current branch is still `main` but there's a new `multiply` branch. Also note
  how immediate it is to create a new branch.
3. Switch to the `multiply` branch and list the local branches again:
  ```
  git checkout multiply
  git branch
  ```
4. Now let's display the history of commits on both branches:
  ```
  git log --decorate --graph --oneline --all
  ```
  You can also try with a graphical tool, such as `gitk` using `gitk --all`
5. Let's add a new multiply function to the `myfuncs.py` module:
  ```python
  def multiply(a, b):
      """Multiply a by b."""
      return a * b
  ```
6. Commit the changes above, they should end up in the `multiply` branch, and
  display the history of changes, like before:
  ```
  git commit -am "myfuncs: add the multiply function"
  git log --decorate --graph --oneline --all
  ```
  The `multiply` branch is now one commit ahead of `main`.
7. Now switch back to the `main` branch, and add the following test function
  to `test_myfuncs.py`:
  ```python
  # Add this import line
  from myfuncs import multiply

  [...]

  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, 0),
          (0, 42, 0),
          (42, 0, 0),
          (42, 1, 42),
          (1, 42, 41),
          (-1, 42, -42),
      ]
  )
  def test_multiply(a, b, res):
      assert multiply(a, b) == res
  ```
8. Finally, commit the changes above and display the branch history:
  ```
  git commit -am "tests: add test function for multiply"
  git log --decorate --graph --oneline --all
  ```
  &#8594; the branches are diverging

Let's go back to the slides to see how to merge them and to fix potential
conflicts.

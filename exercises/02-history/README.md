# Manage the changes

Let's continue with the `dummymaths` Python project and check the history of changes there.


1. Display the history of changes already committed, using `git log`:
  * Only the last 2 changes with `-2` flag along with their corresponding differences using `-p` flag
  * Display the commit information with the format `<small hash> - <message> - <date> - <email>` (check the help of log)
2. Let's extend the tests in `test_myfuncs.py` with a test for the `sub` function, e.g with the following content:
  ```python
  # Add this import line
  from myfuncs import add, sub

  [...]

  # Add this function at the end of the file
  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, 0),
          (0, 42, -42),
          (42, 0, 42),
          (42, 42, 0),
          (42, -42, 84),
          (-42, 42, -84),
      ]
  )
  def test_sub(a, b, res):
      assert sub(a, b) == res
  ```
3. Now completely revert these changes and check the state of your local copy:
  ```
  git checkout .
  ```
4. Apply again the changes above to the `test_myfuncs.py` file, save and add them to the staging area:
  ```
  git add test_myfuncs.py
  ```
  Check the state of your local copy again with `git status`: there's something in the staging area and nothing in the local changes.
  ```shell
  git status
  git diff  # This command should return nothing
  ```
  Verify that the changes added in the staging are the ones expected:
  ```
  git diff --staged
  ```
5. Remove the changes from the staging area:
  ```
  git reset
  ```
  Check the modifications are still there, in the local changes:
  ```
  git status
  git diff
  ```
6. Repeat 4. and 5. but this time completely revert the changes added to the staging area (`git reset --hard`)
7. Apply one last time the changes above to the `test_myfuncs.py` file and commit them:
  ```
  git add test_myfuncs.py
  git commit -m "add test function for sub"
  ```
8. Check the diff contained in this last commit:
  ```
  git log -1 -p
  ```

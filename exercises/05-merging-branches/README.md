# Merging branches

Back to the `dummymaths` Python project, let's create several branches and
merge them in the `main` branch. For this, we'll imagine a (non-realistic)
scenario to write a function implementing division along with a test function.

## The fast-forward merge

1. Let's create the `divide`and the `todo` branches from `main`, which is your current branch:
  ```
  git branch divide
  git checkout -b todo
  git status
  ```
2. You are now on the `todo` branch. Edit the README.md file and add the following content at the end:
  ```
  ### TODO

  - Add _divide_ function
  ```
3. Commit the change above:
  ```
  git commit -am "README.md: bootstrap todo section with a divide function item"
  git status
  git log --decorate --graph --oneline --all
  ```
  The `todo` is now one commit ahead of `main`
4. Switch back to `main` and merge `todo` in main:
  ```
  git checkout main
  git merge todo
  ```
  Git will just "fast-forward `main` to `todo`
  ```
  git log --decorate --graph --oneline --all
  ```
5. As good citizens, now that the `todo` branch is not needed anymore, let's remove 
  it:
  ```
  git branch -d todo
  git log --decorate --graph --oneline --all
  ```

## The merge commit case

At the end of the `04-branches` exercise, the `multiply` and `main` branches
were diverging. Normally the changes introduced in `multiply` are separate
enough from the changes added to main and merging `multiply` in `main `should
not conflict.
Merge the `multiply` branch into `main`:
```
git merge multiply
git log --decorate --graph --oneline --all
```
The merge command created a merge commit.

## The conflict!

1. Now let's trigger a conflict on purpose by finally switching to the `divide` branch:
  ```
  git checkout divide
  ```
2. Add the missing `divide` function to the `myfuncs.py` module:
  ```python
  def divide(a, b):
      """Divide a by b."""
      try:
          return a / b
      except ZeroDivisionError:
          return None
  ```
  commit that change:
  ```
  git commit -am "myfuncs: add divide function"
  ```
3. Add the related test function to the `test_myfuncs.py` module:
  ```python
    # Add this import line
  from myfuncs import divide

  [...]

  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, None),
          (0, 42, 0),
          (42, 0, None),
          (42, 1, 42),
          (1, 2, 0.5),
          (-1, 2, -0.5),
      ]
  )
  def test_divide(a, b, res):
      assert divide(a, b) == res
  ```
  commit that change:
  ```
  git commit -am "tests: add test function for divide"
  ```
4. Now try to merge the `divide` branch in `main`:
  ```
  git checkout main
  git merge divide
  ```
5. Try to solve the conflict! 2 possibilities:
  1. Manually:
    - Fix it by editing the file
    - Run `git add`
    - Run `git commit`
  2. Using a graphical tool:
    ```
    git mergetool
    git add myfuncs.py
    git commit
    ```

## One last thing

Once all features are merged, it's time to sync your local `main` branch with
the remote repository:

```
git push origin main
```

Also, now that the `multiply` and `divide` branches are not needed anymore, you
can delete them:

```
git branch -d multiply divide
```

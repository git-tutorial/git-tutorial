# Simple rebasing

Let's again extend the `dummymaths` Python project with a `power` function.

1. Check that your current branch is `main`:
  ```
  git status
  ```
2. Create a new `power` branch and switch to it:
  ```
  git checkout -b power
  ```
3. Extend the `myfuncs.py` module with the following content:
  ```python
  def power(a, b):
      """Return a power b."""
      return a ** b
  ```
  Commit your change:
  ```sh
  git commit -am "myfuncs: add power function"
  ```
4. Add the related test function to the `test_myfuncs.py` module:
  ```python
  # Add this import line
  from myfuncs import power

  [...]

  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, 1),
          (0, 2, 0),
          (1, 2, 1),
          (2, 0, 1),
          (2, 1, 2),
          (2, 2, 4),
          (2, -1, 0.5),
      ]
  )
  def test_power(a, b, res):
      assert power(a, b) == res
  ```
  Commit your change:
  ```sh
  git commit -am "tests: add test function for power"
  ```
5. Switch back to main
6. Remove the `TODO` section from the README (the divide function is merged already!).
  ```
  git status
  git add README.md
  git commit -m "README: remove TODO section"
  git log --decorate --graph --oneline --all
  ```
  At this point, the `main` and `power` branches have diverged.
7. Switch back to `power` and rebase it on top of `main`:
  ```
  git checkout power
  git rebase main
  git log --decorate --graph --oneline --all
  ```
8. Merge power into `main`:
  ```
  git checkout main
  git merge power
  git log --decorate --graph --oneline --all
  ```
  This is a fast-forward move of main towards power!
9. Finally push `main` and delete `power`:
  ```
  git push origin main
  git branch -d power
  ```

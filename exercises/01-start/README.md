# Start a small Python project


## I. Initialize the project

1. Create a directory `dummymaths`:
  ```
  mkdir dummymaths
  cd dummymaths
  ```
2. Initialize a Git local repository in the `dummymaths` directory:
  ```
  git init
  ```
3. Add a `README.md` file and then check the state of your local copy:
   vérifiez l'état de votre copie locale:
  ```
  git status
  ```
4. Add the README.md file to the staging area and check again the state of your local copy:
  ```
  git add .
  ```
  or, more explicitly:
  ```
  git add README.md
  ```
5. Edit the `README.md` file (for example, add a title, a short description of this small Python project), save and check again the state of your local copy. You should have changes both in the staging area and in the working directory (local changes). Display the changes, first in the staging area and then the local changes:
  ```
  git diff --staged
  git diff
  ```
6. Commit all changes in the `README.md` file (both in staging and local) and check one last time the state of the local copy:
  ```
  git add README.md
  git commit -m "initial commit"
  ```

## II. Ajouter le fichier myfuncs.py

1. Add the file `myfuncs.py` with the following content:
  ```python
  """Some useless mathematical utility functions."""

  def add(a, b):
      """Return the sum of a and b."""
      return a + b

  def sub(a, b):
      """Substract b from a."""
      return a - b
  ```
2. Commit this file:
  ```
  git add myfuncs.py
  git commit -m "initial version of myfuncs.py"
  ```

## III. Ajouter le fichier `test_myfuncs.py` et lancer les tests

1. Add the file `test_myfuncs.py` with the following content:
  ```python
  import pytest

  from myfuncs import add

  @pytest.mark.parametrize(
      "a,b,res",
      [
          (0, 0, 0),
          (0, 42, 42),
          (42, 0, 42),
          (42, -42, 0),
          (-42, 42, 0),
      ]
  )
  def test_add(a, b, res):
      assert add(a, b) == res
  ```
2. Use `pytest` (install it using `pip install pytest`) to run the tests, verify that they pass and then commit `test_myfuncs.py` (and only this one!):
  ```
  pytest -v
  git add test_myfuncs.py 
  git commit -m "tests: add test function for add"
  ```

## IV. Ignore generated files

At this stage, they are Python bytecode generated files displayed when running `git status`. And we don't want to commit them inadvertently: this is the purpose of the `.gitignore` file.

1. Add the `.gitignore` to the base directory of your working copy with the following content:
  ```
  *pyc
  ```
2. Check that bytecode generated files are not listed anymore when running `git status`.
3. Commit the `.gitignore` file:
  ```
  git add .gitignore
  git commit -m "ignore Python generated files"
  ```

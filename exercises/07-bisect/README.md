# Using bisect to find bugs

The `dummymaths` Python project provides a set of unit tests. In the first
exercise you used `pytest` to run them.
Since then, new code was added but you didn't rerun `pytest`.

1. Let's start by running `pytest`. It should fail. If it doesn't this exercise
  becomes useless!
  ```sh
  cd dummymaths
  pytest
  ```
2. Even if the problem is obvious, you'll use `git bisect` to find the commit
  that introduced the problem. In some more complex cases, that can help
  understand what is the origin of the bug.
  Before starting bisect, you must find a commit that works. We know one: the
  one that contains the "add" function test that was added in exercise
  `01-start`. Let's use `git log` with some special parameters to find it:
  ```sh
  git log --oneline --grep "tests: add test function for add"
  ```
  The reply contains the short hash of the matching commit that you'll use as
  good commit.
3. Let's now start bisecting:
  ```
  git bisect start
  git bisect bad
  ```
4. You now have to switch the commit that we think is good, test it and tell git:
  ```
  git checkout <short commit hash returned in point 2.>
  pytest
  git bisect good
  ```
5. Since `pytest` is the command that is used to check if a commit is good or
  not, you can run it at each step:
  ```
  git bisect run pytest
  git bisect reset
  ```
  This command should tell you quite fast what is the first bad commit. Check
  it's content:
  ```
  git show <first bad commit>
  ```
6. Point 5. reveals that the problem comes from one the multiply test case. It
  can be fixed by using the following parametrize values:
  ```python
    [
        (0, 0, 0),
        (0, 42, 0),
        (42, 0, 0),
        (42, 1, 42),
        (1, 42, 42),  # problem was here
        (-1, 42, -42),
    ]
  ```
7. Commit your changes and push the `main` branch:
  ```
  git commit -am "tests: fix multiply test case"
  git push origin main
  ```

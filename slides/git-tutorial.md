class: center, middle

# **Git**

## Beginner Tutorial

https://gitlab.inria.fr/git-tutorial/git-tutorial

<img src="images/inria_logo.png" alt="Logo" style="width: 150px;"/><br/>

---

class: left, middle

## Schedule


##### _Morning_

#### <span style="margin-left:5em">1. Introduction</span>

#### <span style="margin-left:5em">2. The basic commands</span>

#### <span style="margin-left:5em">3. Working with branches</span>

##### _Afternoon_

#### <span style="margin-left:5em">4. Advanced usage</span>

#### <span style="margin-left:5em">5. Working with GitLab and GitHub</span>

---

class: left, middle

#### <span style="margin-left:5em">1. Introduction</span>

##### <span style="margin-left:7em">1.1. About version control systems (VCS)</span>

##### <span style="margin-left:7em">1.2. Git features</span>

##### <span style="margin-left:7em">1.3. How it works</span>

##### <span style="margin-left:7em">1.4. The 3 states</span>

##### <span style="margin-left:7em">1.5. Structure of a Git repository</span>

---

## About version control

- Maintaining an history of changes

- Different version control modes: __Local, Centralized, Distributed__

- Examples of centralized version control tools: CVS, Subversion

- Examples of distributed version control tools: Mercurial, Git

.center[
<img src="images/centralized.png" alt="Centralized" style="width:250px;"/>&nbsp;<img src="images/distributed.png" alt="distributed" style="width:250px;"/>
]

.center[[Reference: Git Book](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control)]

---

## Git main features

- Extremely __fast__

- Supports __non linear development__ (branches)

- __Distributed__: the whole repository is duplicated by each developer

- The version control remains __local__: no network connection required

- Allows to maintain __very large codebases__

.center[
  <img src="images/distributed.png" alt="distributed" style="width:250px;"/>
]

---

## How it works

.center[__Git maintains snapshots__]

<table style="width:100%">
<tr>
  <td><b>Diffs</b></td>
  <td><img src="images/deltas.png" alt="Deltas" style="width:500px;"/></td>
</tr>
<tr>
  <td><b>Snapshots</b></td>
  <td><img src="images/snapshots.png" alt="snapshots" style="width:500px;"/></td>
</tr>
</table>

---

## The 3 states

* __Modified__ : <span style="color:red">Working directory</span>
* __Staged__  : <span style="color:green">Staging area</span>
* __Committed__  : Integrated to the repository database

.center[
  <img src="images/areas.png" alt="areas" style="width:500px;"/>
]

Check the current states of the local copy:
```shell
$ git status
```

---

## Structure of a Git repository

```
<Local copy>
├── .git
│   ├── branches       # Contains the list of branches
│   ├── config         # Local configuration file
│   ├── description    # File containing the repository name
│   ├── HEAD           # Pointer to the current commit
│   ├── hooks
│   ├── index          # File containing the changes in staging
│   ├── info
│   │   └── exclude    # Global file containing ignored patterns
│   ├── objects        # Objects database managed by Git
│   │   ├── info
│   │   └── pack
│   └── refs           # All known references contained in the local repository
│       ├── heads
│       └── tags
└── <working directory>
```
Each object is identified by a SHA-1 checksum<br/>
<br/>
.right[__&#8594; guarantees data integrity__]

---

## Different ways to use Git

* Using the command line interface: gives access to __all__ features
```
$ git help <command>  # display manual of a particular command
```

* Graphical user interfaces (gitk, git-gui, qgit, ungit, etc)

* Directly integrated to IDEs


&#8594; __We will use the command line interface__

<br/>
Setup:

* Linux :
```
sudo apt-get install git-all
sudo yum install git-all
```
* Windows: [Git for Windows](https://git-for-windows.github.io/)

* MacOS: [Git setup](http://git-scm.com/download/mac)

---

class: center

## Summary

How it works, Setup, Different ways to use Git

<img src="images/snapshots.png" alt="snapshots" style="width:500px;"/>

<img src="images/areas.png" alt="areas" style="width:400px;"/>

---

class: left, middle

#### <span style="margin-left:5em">2. The basic commands</span>

##### <span style="margin-left:7em">2.1. Configuring Git</span>

##### <span style="margin-left:7em">2.2. Initializing a new repository</span>

##### <span style="margin-left:7em">2.3. Adding changes</span>

##### <span style="margin-left:7em">2.4. Tracking the history of changes</span>

##### <span style="margin-left:7em">2.5. Reverting changes</span>

##### <span style="margin-left:7em">2.6. Working with remotes</span>

---

## Configuration


* Use simple configuration files

* 3 levels:
  * __local__ : `<local copy>/.git/config`, by default
  * __global__ : `~/.gitconfig`, option `--global`
  * __system__ : `/etc/gitconfig`, option `--system`

* Either edit the files directly, or use `git config` command
```
$ git help config
$ git config --edit            # edit local parameters
$ git config --global --edit   # edit global parameters
```

Always start by filling your user name and email, at the __global__ level:
```
$ git config --global user.name "First Last"
$ git config --global user.email your.email@organisation.com
```

---

## Exercises : install et configure git

1. If not done already, install Git

2. Globally configure your preferred editor (option `core.editor`)

3. Globally configure your user name and email

4. Add a few `alias` (examples: alias.st=status, alias.conf=config, etc)

<br/><br/><br/>
<br/><br/><br/><br/><br/>
<br/><br/><br/><br/><br/>
.right[Aide : `git config --global <paramètre> <valeur>`]

---

## Initialize a new repository

* Start to version code with Git using a single command in the base directory:
```
$ git init
```

* When in server mode: __no working directory__
```
$ git init --bare <repository directory>.git
```

The __`.git`__ extention is used as a convention

.center[
  <img src="images/bare.svg" alt="bare" style="width:400px;"/>
]

---

## Add changes

* Add changes to the index ("Stage Fixes") &#8594; Staged
```
$ git add <list of files> or <list of directories>
```

* Add changes to the local copy ("Repository") &#8594; Committed
```
$ git commit -m "commit message"
```

* Remember to use `git status` to check the state of your local copy
.center[
  <img src="images/areas.png" alt="areas" style="width:550px;"/>
]

---

## Add changes: some tricks

* Skip the staging area step using option `-a`
```
$ git commit -a -m "message de commit"
```

* Add partial changes to the staging area using option `-p`
```
$ git add -p <list of files>
```
Then type _y_ or _n_ to choose which blocks of changes to stage

---

## Managing files
.center[
  <img src="images/lifecycle.png" alt="areas" style="width:500px;"/>
  <br/><b>File status life cycle</b>
]

* Delete a list of files from the working directory and put this change in the staging area: `$ git rm <list of files>`

* Rename/move a file : `git mv <src> <dst>`

* Remember the `.gitignore` file at the base directory of the working directory to ignore file/directory patterns from the _untracked_ state

* _untracked_ files/directories are treated like regular files/directories

---

## Track differences between the 3 states

This is simply done using `git diff`

* By default, `git diff` &#8594; shows the differences between the working directory and the staging area or,
  when the staging area is empty, the last commit

* `git diff --staged`    &#8594; shows the differences between the staging area and the last commit

* `git diff <commit>`    &#8594; shows the differences between the working directory and a commit

* `git diff <commit1> <commit2>` &#8594; shows the differences between 2 commits

<pre style='font-size:16px;margin-left:100px'>
$ git diff
<b>diff --git a/README.txt b/README.txt
index e69de29..8fd7633 100644
--- a/README.txt
+++ b/README.txt</b>
@@ 0 -1,0 +1 @@
<span style="color:red">-Previous content</span>
<span style="color:green">+New content</span>
</pre>

---

## Exercises: start a small Python project

Let's discover Git usages by writing a small Python project implementing simple mathematical functions.

This is how the project organization will look like in the end:

<pre style="margin-left:200px">
├── .gitignore
├── myfuncs.py
├── README.md
└── test_myfuncs.py
</pre>

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/01-start" target="_blank" rel="noreferrer noopener">01-start</a>

---

## Track the history of a local copy

* Using a graphical user interface: `gitk`

* `git log` displays the history of commits

* Some useful `git log` options:

```
 -n                # Limit the displayed history to the last n commits
 -p                # Display the changes contained in each commit
 --pretty=oneline  # Each commit information is displayed in one oneline
 --pretty=format:"<rule>" # Finely tune the pattern used to display the information
 --graph           # Display the history of commits as a tree (see branches later)
```

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

.right[Useful: Configuration option `pager.log=less` to activate pagination]

---

## Revert changes

3 useful commands:
* `git commit --amend` &#8594; modify the last commit

* `git reset`
    * revert the last commit if the staging area is empty &#8594; the changes in the commit are kept in the working directory
    * with the `--hard` flag: changes are totally removed and thus lost (__watch out!__)
    * remove changes from the staging area if there are any
    * `-p` flag: select changes to revert

* `git checkout`: reload the last commit in the working directory (__watch out!__)
    * `git checkout <file or directory>`: revert changes on a file/directory
    * `git checkout <commit> <file or directory>`: load in the staging area the diff between the current version and the commit version of the file/directory
    * `-p` flag: select changes to cancel

---

## Exercises: manage changes

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/02-history" target="_blank" rel="noreferrer noopener">02-history</a>.

---

## Working with remote repositories

__Principle__ :<br/>
 * All developers own more or less synchronized local copies of a Git repository

 * Only committed changes can be exchanged

 * Information about remote repositories are available using `git remote`

.center[
<img src="images/distributed.png" alt="distributed" style="width:280px;"/>
]

---

## Cloning a remote repository

* __git clone__ &#8594; one gets __a local copy__ of a remote repository
```
$ git clone <repository url>
```

* There are several types of __url__:
 * A local directory containing a bare repository (e.g initialized using `--bare`)

 * A read-only repository on a remote host:
     ```
     git://host/<path to the repository>
     ```

 * Read/write access, depending on the rights, from a remote host:
     ```
     ssh://user@host:<path to the repository>

     git@host:<path to the repository>

     https://host/<path to the repository>
     ```
* By default, Git loads the `main` branch locally (formerly it was `master`)

---

## Introduction to remote repositories

* They are identified by their __url__ and __name__

* By default, when cloning a repository, the remote is called __origin__.

* `git remote` displays the list of remote repositories, by name

* `git remote -v` gives more details (such as the url)

* To add a remote repository: `git remote add <name> <url>`

* Other useful commands:
    * `git remote rm <name>`: delete the remote repository (__!locally__)
    * `git remote rename <old> <new>`: rename the remote repository
    * `git remote show <name>`: inspect the remote repository

---

## Synchronize with a remote repository

* Push local changes to a remote repository:
```
$ git push <remote name> <branch>
```

* Locally get the updates from a remote repository:
```
$ git fetch <remote name>
```

* Fetch changes from a remote and integrates them in current branch:
```
$ git pull <remote name> <branch>
```
---

## Exercises: some manipulations with remote repositories

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/03-remotes" target="_blank" rel="noreferrer noopener">03-remotes</a>.

---

## Summary: the basic commands

So far we have seen how to locally manage changes using Git, in particular:

* How to configure Git

  &#8594; `git config`, options `--global`, `--edit`

* How to initialize a local repository and how to interact with remote repositories

  &#8594; `git init`, `git clone`, `git pull`, `git push`, `git fetch`

* How to manage changes between the 3 local states: modified, staged and committed

  &#8594; `git add`, `git commit`

* How to cancel/revert local changes

  &#8594; `git commit --amend`, `git reset`, `git checkout`

* How to track local changes

  &#8594; `git status`, `git diff`, option `--staged`, `git log`

---

class: left, middle

  #### <span style="margin-left:5em">3. Working with branches</span>

  ##### <span style="margin-left:7em">3.1. What is a branch?</span>

  ##### <span style="margin-left:7em">3.2. Starting a new branch</span>

  ##### <span style="margin-left:7em">3.3. Switching branches</span>

  ##### <span style="margin-left:7em">3.4. Merging branches</span>

  ##### <span style="margin-left:7em">3.5. Resolving conflicts</span>

  ##### <span style="margin-left:7em">3.6. Remote branches</span>

---

## More insight on commits

Main information stored in a commit is:
 * Metadata: author name, date, message
 * Pointer to the content snapshot
 * Pointer(s) to ancestor commit(s)

```shell
$ git add README test.rb LICENSE
$ git commit -m 'initial commit of my project'
```

.center[
<img src="images/commit-and-tree.png" alt="commit-and-tree" style="width:550px;"/>
]

---

## More insight on commits

The history corresponds to a linked list of commits:

.center[
<img src="images/commits-and-parents.png" alt="commit-and-parents" style="width:750px;"/>
]

---

## What is a branch?

* __One branch = one pointer__ to a commit

* For each new commit, the pointer automatically moves forward

* More precisely, __a branch = a file__ which name is the branch name and containing the commit hash (SHA-1)

.center[
<img src="images/branch-and-history.png" alt="branch-and-history" style="width:600px;"/>
]

---

## Reminder: Internal structure of a Git repository

```
<Local copy>
├── .git
│   ├── branches       # Contains the list of branches
│   ├── config         # Local configuration file
│   ├── description    # File containing the repository name
│   ├── HEAD           # Pointer to the current commit
│   ├── hooks
│   ├── index          # File containing the changes in staging
│   ├── info
│   │   └── exclude    # Global file containing ignored patterns
│   ├── objects        # Objects database managed by Git
│   │   ├── info
│   │   └── pack
│   └── refs           # All known references contained in the local repository
│       ├── heads
│       └── tags
└── <working directory>
```

---

## Some notations

* `HEAD` &#8594; current version loaded in the working directory

* `HEAD^ or HEAD~` &#8594; first parent of the `HEAD` commit

* `HEAD~n` &#8594; n-th parent of the `HEAD` commit

* `Detached HEAD` &#8594; the current position of `HEAD` doesn't correspond to any known branch

* `main` &#8594; name of the default branch (formerly, it was `master`)

* List branches:
```sh
$ git branch    # display local branches, the current branch is
                    # preceded by *
$ git branch -a # display all branches, local and remote
```

---

## Starting a new branch

* Creating a new branch is very fast: it just creates a single file

* `git branch <new branche>` &#8594; create a new pointer to the current commit

* `git log --oneline --decorate` &#8594; displays the history with known branches

Example:
```shell
$ git branch testing
```

.center[
<img src="images/head-to-master.png" alt="head-to-master" style="width:450px;"/>
]

---

## Basic operations on branches

`git branch`: base command for managing branches

* Rename a branch: <br/>`git branch -m <old name> <new name>`

* Delete a branch: <br/>`git branch -d <branch name>` &#8594; No risk. The branch is deleted only if it's up-to-date with its _uptream_ version

* Delete a branch: <br/>`git branch -D <branche>` &#8594; Force branch deletion

* Display branches that are merged/unmerged in the current branch: <br/> `git branch --merged/--no-merged`

* Display the hash and comment of local branches: `git branch -v`

---

## Switching branches

* `git checkout <branch>` &#8594; move `HEAD` to the commit pointed by `<branch>`
* `git checkout` also loads the snapshot of the commit in the working directory
* `git switch <branch>` &#8594; available in recent versions of Git

Example:
```sh
$ git checkout testing
```

.center[
<img src="images/head-to-testing.png" alt="head-to-testing" style="width:300px;"/>
]

* use `-b` flag to create and automatically switch to the newly created branch

---

## Switching branches

__Watch out!__: switching branches changes the content of the working directory

<table style="width:100%">
<tr>
  <td><pre>$ vim test.rb
$ git commit -a -m 'some change'
  </pre></td>
  <td><img src="images/advance-testing.png" alt="advance-testing" style="width:400px;"/></td>
</tr>
<tr>
<td><br/><br/></td>
<td></td>
</tr>
<tr>
  <td><pre>$ git checkout main</pre></td>
  <td><img src="images/checkout-master.png" alt="checkout-master" style="width:400px;"/></td>
</tr>
</table>

---

## Diverging branches

Let's add a new commit in `main`:
```sh
$ vim test.rb
$ git commit -a -m 'other changes'
```
.center[
<img src="images/advance-master.png" alt="advance-master" style="width:500px;"/>
]
* The diverging branch can be seen using `git log --oneline --decorate --graph --all` or with `gitk --all`

---

## Exercises: Manipulating branches

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/04-branches" target="_blank" rel="noreferrer noopener">04-branches</a>

---

## Merging branches

2 cases:
* One branch (`branch1`) is the starting point of another branch (`branch2`)

&#8594; Merging `branch2` in `branch1` = __fast-forward__ move of `branch1` to `branch2`

* Branches `branch1` et `branch2` have diverged

&#8594; Merging `branch2` in `branch1` = create a merge commit

<br/>
__Merge commit__ : commit with 3 ancestors
* ancestor of _branch1_
* ancestor de _branch2_
* common ancestor of _branch1_ and _branch2_

<br/>

Let's have a closer look to an example: one wants to hot fix a _bug_ in main while working on another branch

---

## Merging branches: workflow example

<table style="width:100%">
<tr>
  <td>1. One starts from <i>main</i><pre>git checkout main</pre>
  </td>
  <td><img src="images/basic-branching-1.png" alt="basic-branching-1" style="width:200px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>2. One creates and switches to a branch <i>iss53</i><pre>git checkout -b iss53</pre>
  </td>
  <td><img src="images/basic-branching-2.png" alt="basic-branching-2" style="width:200px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>3. One commits changes (C3) in branch <i>iss53</i>
    <pre>git commit -a -m "new feature"</pre></td>
  <td><img src="images/basic-branching-3.png" alt="basic-branching-3" style="width:200px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>4. One switches back to <i>main</i> and switches to the new <i>hotfix</i> branch
    <pre>git checkout main
git checkout -b hotfix
    </pre></td>
  <td><img src="images/basic-branching-4.png" alt="basic-branching-4" style="width:200px;"/></td>
</tr>
</table>

---

## Merging branches: workflow example

<table style="width:100%">
<tr>
  <td>5. When merging <i>hotfix</i> into `main` &#8594; fast-forward move of `main` towards `hotfix`
  <pre>git checkout main
git merge hotfix
  </pre></td>
  <td><img src="images/basic-branching-5.png" alt="basic-branching-5" style="width:250px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>6. One commits a new change in `iss53` branch.
    <pre>git checkout iss53
git commit -a -m "another change"</pre></td>
  <td><img src="images/basic-branching-6.png" alt="basic-branching-6" style="width:250px;"/></td>
</tr>
</table>

---

## Merging branches: workflow example

<table style="width:100%">
<tr>
  <td>7. <i>iss53</i> (C5) and <i>main</i> (C3) common ancestor is C2.</td>
  <td><img src="images/basic-merging-1.png" alt="basic-merging-1" style="width:250px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>8. Merging <i>iss53</i> in <i>main</i> creates a merge commit C6.
    <pre>git checkout main
git merge iss53</pre></td>
  <td><img src="images/basic-merging-2.png" alt="basic-merging-2" style="width:250px;"/></td>
</tr>
</table>

---

## Merging branches: how to solve conflicts

Git is very good at merging things, but _sometimes_ it's not able to do it alone
- Example of a merge attempt producing a conflict:
```
$ git merge conflicting_branch
Auto-merging README.txt
CONFLICT (content): Merge conflict in README.txt
Automatic merge failed; fix conflicts and then commit the result
```

&#8594; 2 ways to solve the conflict:
* edit the files by hand
* use `git mergetool`<br/>
_Tips_ :
```sh
git config --global merge.tool <your favorite merge conflict tool>
```
Some existing tools: meld, kdiff3, tortoisegit, vimdiff, etc

&#8594; `git commit` once the conflict is solved

&#8594; `git merge --abort` abort the merge attempt (when things go wrong)

---

## How to solve conflicts

* Manually solving the conflict:
```
<<<<<<< HEAD
Content from HEAD => the current version in the working directory
 =======
Content from the conflicting_branch branch
 >>>>>>> conflicting_branch
```

* Solving using a graphical tool:
.center[
<img src="images/meld.png" alt="meld" style="width:700px;"/>
]

---

## Exercises: merging branches

Follow instructions in the README.md file <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/05-merging-branches" target="_blank" rel="noreferrer noopener">05-merging-branches</a>.

---

## Remote branches

* These special branches are pointers to branches on remote repositories

* They are named following the `<repository>/<branch>` name pattern. Example: `origin/main`

* Display the list of remote branches: `git branch -a` or `git remote show <dépôt>`

* These branches are __local__ to the local repository &#8594; use `git fetch <repository>` to synchronize them (option `--all` will sync all repositories)

* They cannot be modified &#8594; they don't follow new commits

.center[[Example](https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches)]

---

## Tracking remote branches

* By default, `git clone` automatically sets the `main` branch to track `origin/main`

* But a branch created locally (`git branch <branch>`) and then pushed (`git push <remote> <branch>`) won't _track_ its remote branch by default<br/>
&#8594; `git pull` without options won't work<br/>
&#8594; automatic track must be enabled using the `-u` flag during the first push

* Activate the tracking when creating a branch:
    * `git branch -u origin/<branch>` &#8594; creates `<branch>` with tracking of `origin/<branch>` enabled
    * `git checkout --track origin/<branch>` &#8594; creates `<branch>` and switches to it
    * `git checkout -b <local branch> origin/<branch>` &#8594; creates `<local branch>` from `origin/<branch>` and switches to it

* Example: activate tracking of `origin/main` on local `main` branch:<br/>
`git branch --set-upstream-to=origin/main main`

* _Personal advice_ : be __explicit__, limit the use of bare `pull`/`push` commands

---

## Other actions on remote branches

* Delete a branch on a remote repository:<br/>
`git push <remote> --delete <branch>`<br/>
or `git push <remote> :<branch>`<br/>
&#8594; but the local branches still exist

* Remove local references (`<remote>/<branch>`) deleted in remote repository
(! `-n` option for dry-run mode):<br/>
`git remote prune -n <remote>`

---

## Some advice on how to manage your branches

* When starting a new feature, branch from `main`

* Before creating a new branch, sync the base branch with the remote (e.g. fetch)

* Follow the idiom: One branch per feature or fix

* When unsure, before merging, create a backup branch in case things go wrong

---

## Summary

In this (long) section, we learned the principle and usage of branches with Git, in particular:

* A branch is just a file with a commit hash

* `HEAD` is a pointer to the commit loaded in the working directory

* How to start a new branch: `git branch <branch>`

* How to switch on a new branch: `git checkout <branch>`

* How to display the history of all branches using `git log --all` or `gitk --all`

* How to merge branches

* Some manipulations with remote branches

---

class: left, middle

  #### <span style="margin-left:5em">4. Advanced usage</span>

  ##### <span style="margin-left:7em">4.1. Rebasing</span>

  ##### <span style="margin-left:7em">4.2. Debugging using Git</span>

  ##### <span style="margin-left:7em">4.3. Worktrees</span>

---

## Rebasing

* It is another way to merge branches

* Rebase __applies__ successively each commit of a branch to another branch

* To merge a branch in the current branch, use `git rebase <branch>`

<table style="width:100%">
<tr>
  <td>Consider this starting point:<br/>
  <i>experiment</i> et <i>main</i> have diverged
  </td>
  <td><img src="images/basic-rebase-1.png" alt="basic-rebase-1" style="width:250px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td>
  <pre>git checkout experiment
git rebase main
  </pre></td>
  <td><img src="images/basic-rebase-3.png" alt="basic-rebase-3" style="width:250px;"/></td>
</tr>
</table>

&#8594; The rebase created a new commit `C4'` <br/>
&#8594; _main_ is now the starting point of _experiment_

---

## Rebasing principle

&#8594; It's now possible to fast-forward _main_ to _experiment_:

<table style="width:100%">
<tr>
  <td><i>experiment</i> rebased on <i>main</i></td>
  <td><img src="images/basic-rebase-3.png" alt="basic-rebase-3" style="width:250px;"/></td>
</tr>
<tr><td></td><td><hr></td></tr>
<tr>
  <td><pre>git checkout main
git merge experiment
  </pre>
  </td>
  <td><img src="images/basic-rebase-4.png" alt="basic-rebase-4" style="width:300px;"/></td>
</tr>
</table>

__Main interest__ &#8594; we end up with a __linear history__ of `main`, which is IMHO cleaner

&#8594; Rebasing is also useful to _cleanup_ the history of commits

---

## Drawbacks of rebasing et workarounds

* Rebase _applies_ each commit of a branch to the current branch<br/>
    &#8594; all commits applied have changed<br/>
    &#8594; we get a __different branch__, as it's now pointing at a different commit

* Compared to its remote branch, the rebased branch has __diverged__ and merging it is hard

* To avoid problems:<br/><br/>
__Do not rebase commits that exist outside your repository and that people may have based work on (git book advice)__<br>
<br>
... _unless you are confident and know what you are doing (and nobody added local changes on the same branch_)
<br/>
<br/>
&#8594; _Personal advice_ : you can rebase an already pushed branch if you are the only one working on it. But you'll have to overwrite the remote branch<br/>
```sh
$ git push <remote> <rebased branch> -f  # you have to force the push
```

---

## Interest of rebasing

* Improve the history of commits &#8594; each commit is meaningful

* Reflect better the global history of the project

<table style="width:100%">
<tr>
  <td><img src="images/perils-of-rebasing-4.png" alt="perils-of-rebasing-4" style="width:400px;"/>
  </td>
  <td><img src="images/perils-of-rebasing-5.png" alt="perils-of-rebasing-5" style="width:400px;"/></td>
</tr>
<tr>
  <td><center>History using merge</center></td>
  <td><center>Linear history using rebase</center></td>
</tr>
</table>

---

## Typical rebase workflow

Example:
<pre style='background:lightgrey;margin:10px;padding:5px'>
$ git checkout main             # current branch is main
$ git pull origin main          # sync main
$ git checkout -b new_feature   # start a new feature branch
.
...    # lots of new commits
...    # in the meantime main also evolved eventually
.
$ git pull origin main --rebase    # automatic rebase on
.                                  # latest main
$ git push origin new_feature      # the new_feature branch
.                                  # is up-to-date with main
.                                  # => allows fast-forward
</pre>

---

## How to solve rebase conflicts

* Rebase successively __applies__ each commit of a branch to the current branch
&#8594; conflicts can happen at each step:
<pre style='font-size:14px;background:lightgrey;margin:10px;padding:5px'>
$ git rebase conflicting_branch
First, rewinding head to replay your work on top of it...
Applying: modification dans branche_conflit
Using index info to reconstruct a base tree...
M	README.txt
Falling back to patching base and 3-way merge...
Auto-merging README.txt
CONFLICT (content): Merge conflict in README.txt
</pre>

* How to proceed:
<pre style='font-size:14px;margin:10px;padding:5px'>
$ git rebase <i>branch</i>  # rebase on <i>branch</i>
. <i>as long as there are conflicts:</i>
$ git mergetool   # fix the conflict
$ git rebase --continue
</pre>

* Abort while rebasing: `git rebase --abort`

---

## Change history

* Rebase successively __applies__ each commit of a branch to the current branch<br/>
&#8594; it is possible to change the order, choose the commits to apply

* Use `-i` flag to use the _interactive_ mode

* Then a terminal interface will open which allows to:
    * _edit_: modify the commit message
    * _remove_: remove a commit from the history
    * _squash_: 2 commits are merged together
    * _move_: the order of commits is changed

* `git rebase -i <commit-hash>` applies commits after `<commit-hash>`

---

## Exercise

Following the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/06-rebasing" target="_blank" rel="noreferrer noopener">06-rebasing</a>.

---

## Debug using Git

* Git provides a tool to perform dichotomic search in commits
&#8594; `git bisect`

* Principle:
    1. Initialization:
<pre style='font-size:14px;background:lightgrey;margin:10px;padding:5px'>
$ git bisect start        # initialize on the
.                         # current commit
$ git bisect bad          # tell git it is bad
$ git checkout &lt;hash&gt;     # load working commit
$ git bisect good &lt;hash&gt;  # tell git it is ok
</pre>

    2. Then alternate `git bisect good/bad` depending on the state of the proposed commit

    3. Terminate using `git bisect reset` to switch back to the initial commit

* The whole workflow can be automated:
<pre style='font-size:14px;background:lightgrey;margin:10px;padding:5px'>
$ git bisect start <bad> <good>
$ git bisect run &lt;check command&gt;
$ git bisect reset    # once done
</pre>

---

## Exercises: debug your code using git bisect

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/07-bisect" target="_blank" rel="noreferrer noopener">07-bisect</a>

---

## Check out multiple branches using worktree

* `git worktree` allows you to manage multiple working trees attached to the same repository

* _worktrees_ have additional metadata to differenciate them from other worktrees

* `git init` or `git clone` creates the _main_ worktree

* Added worktree are called _linked_ worktree

* Adding a worktree:
  * `git worktree add ../<branch>` &#8594; creates a worktree in `../<branch>` path with a new branch called `<branch>`
  * `git worktree add <path> <branch>` &#8594; creates a worktree from an existing branch

* Once done with a worktree, it can be removed: `git worktree remove <worktree_name>`

* Use `git worktree list` to get the list all worktrees

---

## Summary

In this section, we've seen several advanced but very useful usage of Git:
* The rebasing to avoid merge commit and to keep a linear and clean history

* How to change the history of commits using interactive rebase, e.g. `git rebase -i`

* How to search in the history of commits a change that introduced a bug

* How to checkout multiple branches at the same time using `git worktree`

---

class: left, middle

  #### <span style="margin-left:5em">5. Working on GitHub/GitLab</span>

  ##### <span style="margin-left:7em">5.1. Possible Git workflows</span>

  ##### <span style="margin-left:7em">5.2. Using forks</span>

  ##### <span style="margin-left:7em">5.3. Overview of GitLab/GitHub</span>

  ##### <span style="margin-left:7em">5.4. Opening a Pull-Request/Merge-Request</span>

  ##### <span style="margin-left:7em">5.5. Code reviews</span>

---

## Possible Git workflows: NoFlow

<table style="width:100%">
<tr>
  <td>All developers are working on the same repository
  </td>
  <td><img src="images/centralized_workflow.svg" alt="" style="width:350px;"/></td>
</tr>
<tr>
  <td>All developpers are working on the same branch
  </td>
  <td><img src="images/no-flow.png" alt="" style="width:400px;"/></td>
</tr>
</table>

Several problems:<br/><br/>
&#8594; All developers need write (e.g. push) access to the main repository<br/>
&#8594; Requires conventions on branch name to avoid name clash<br/>
&#8594; Doesn't scale to large teams<br/>

---

## Possible Git workflows: GitHub Flow

.center[
  <img src="images/github-flow.png" alt="" style="width:350px;"/>
]

- New work is started by branching from main

- Once done, work is reviewed and tested before merging in main

- Simple and allows to release frequently

---

## Possible Git workflows: GitFlow

.center[
  <img src="images/git-flow.png" alt="" style="width:400px;"/>
]

- Suited for projects that have a scheduled release cycle

- Main is your rolled out production code with tagged versions

- Only hotfix, and release branches get merged into main

- Feature branches are merged into develop

- Only bugfixes, not new features, are merged into release branches

---

## Possible Git workflows: GitLab Flow

.center[
  <img src="images/gitlab-flow.png" alt="" style="width:400px;"/>
]

- Ideal workflow for organizations that need to release frequently

- Base workflow similar to GitHub Flow

- `main`: everyone's local development environment line

- `staging`: where `main` is merged into for last minute tests before going to production

- `production`: tagged production code that staging is merged into


---

## Possible Git workflows

Some useful references:

- https://blog.programster.org/git-workflows

- https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow

- https://betterprogramming.pub/a-simple-git-repository-strategy-93a0c7450f23

---

## Using forks

* A main remote repository where only maintainers have write access

* Each developer owns 2 repositories:
    * A local _private_ repository &#8594; its local copie
    * A remote repository &#8594; the remote server which contains a fork of the main repository

* Each developer publish branches to its remote repository

* Maintainers can push (or merge) branches to the main repository

.center[
<img src="images/forking-workflow.svg" alt="basic-rebase-4" style="width:400px;"/>
]

---

## Advantages of forks

* Provides more flexibility

* Scales better to large teams

* Third-party contributions are possible without push rights

* Each developer manages its own fork at will

---

## Working workflow with forks

Let's go contribute to a new project!

1. I clone the upstream repository locally: <br/>`git clone <upstream url>.git`

2. I fork the official upstream repository using the web UI (GitHub/GitLab/Gitea)

3. I add my fork as a remote: <br/>`git remote add <my fork name> <fork url>.git`

4. I create new branches for my various features (`git checkout -b <branch>`). Once done, I check them (self-review, test, iterate).

5. I eventually rebase locally on the latest upstream `main`:<br/>
`git pull upstream main --rebase`. Once done, I check them (self-review, test, iterate).

6. I push my branch(es) to my fork: `git push origin <branch>`

7. I open a _Merge/Pull Request_ explaining what my change is about and wait for CI and reviews
---

## Some advice

* You can use the following remote name conventions:
    * the remote of my fork is called `<my username>`
    * the upstream remote is called `origin`
* I almost never use the _merge_ command, only _rebase_ &#8594; __I'm alone on my fork!__<br/>

Example:
<pre style='font-size:14px;background:lightgrey;margin:10px;padding:5px'>
$ git checkout my_branch     # my working branch
$ git fetch origin           # I sync with upstream
$ git rebase origin/main     # make sure my working branch
.                            # is based on latest main
$ git push origin my_branch  # -f if it was already pushed
</pre>

* Never create a pull request using the `main` branch of your fork targetting the `main` branch upstream

* One branch per feature/fix &#8594; focused changes are easier to review and this keeps your work clean!

---

## Using GitHub and GitLab

__Prerequisites__: create an account and add its public SSH key

Demonstration:

* [GitHub](https://github.com)

* [GitLab](https://gitlab.inria.fr)

---

## Exercises: Working on GitLab

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/08-gitlab" target="_blank" rel="noreferrer noopener">08-gitlab</a>

---

## Exercises: Working on GitHub

Follow the README.md file in <a href="https://gitlab.inria.fr/git-tutorial/git-tutorial/tree/main/exercises/09-github" target="_blank" rel="noreferrer noopener">09-github</a>

---

## Summary

In this last section, we've learned:

* The possible working workflows with Git

* In particular, how to use the forks, with some personal advices

* How to contribute on GitLab and on GitHub

* We opened merge requests on GitLab and briefly went through code reviews

---

class: center, middle

# **Thank You!**

---

#### Detailed schedule (1):

1. Introduction

  1. About version control systems (VCS)
  2. Git features
  3. How it works
  4. The 3 states
  5. Structure of a Git repository

2. The basic commands

  1. Configuring Git
  2. Initializing a new repository
  3. Adding changes
  4. Tracking the history of changes
  5. Reverting changes
  6. Working with remotes

---

#### Detailed schedule (2):

3. Working with branches

  1. What is a branch?
  2. Starting a new branch
  3. Switching branches
  4. Merging branches
  5. Resolving conflicts
  6. Remote branches

4. Advanced usage
  1. Rebasing
  2. Debugging using Git
  3. Worktrees

5. Working on GitHub/GitLab
  1. Team working: possible workflows
  2. Using forks
  3. Overview of GitLab/GitHub
  4. Opening a Pull-Request/Merge-Request
  5. Code reviews

---

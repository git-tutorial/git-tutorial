# Git - Beginner Tutorial

This tutorial will teach you the basics of Git, some advanced usage and how to
work with GitLab and GitHub.

Online slides are available [here](https://git-tutorial.gitlabpages.inria.fr/git-tutorial)

Slides in pdf are also available [here](git-tutorial-en.pdf)

## Credits

This tutorial reuses material from:
* [Git book](https://git-scm.com/book/en/v2)
* [Atlassian tutorials](https://fr.atlassian.com/git/tutorials)
* [GitHub online tutorial](https://try.github.io)

Other useful online resources:

* [Git user manual](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/user-manual.html)
* [A Grip on Git](https://agripongit.vincenttunru.com/)
* [Oh Shit, Git!?!](https://ohshitgit.com/)
* Web based interactive experience with git:
  * [Explain git with D3](https://onlywei.github.io/explain-git-with-d3/)
  * [Learn git branching](https://learngitbranching.js.org/)


## License

This tutorial is licensed under the Creative commons license
[CC-BY-ND](https://creativecommons.org/licenses/by-nd/4.0/)

![CC-BY-ND](https://mirrors.creativecommons.org/presskit/buttons/80x15/png/by-nd.png)
